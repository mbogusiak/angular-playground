import {Component} from '@angular/core';
import {ProductService} from '../service/product-service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
  providers: [ProductService]
})

export class ProductDetailsComponent {
  display: boolean = false;

  showDialog() {
    this.display = true;
  }
}
