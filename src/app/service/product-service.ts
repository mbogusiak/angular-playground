import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

@Injectable()
export class ProductService {

  constructor(private http: Http) {}

  public getProducts(): Observable<any> {
    return  this.http.get('http://angular-products.herokuapp.com/products').map(res => res.json());
  }
}
