#Before you start
 - install latest nodejs => https://nodejs.org/en/
 - install Angular CLI => https://github.com/angular/angular-cli
 - Check endpoints=> Angular-playground.postman_collection.json (server sleeps down after about 1h of inactivity)

#To run project
 - npm install
 - npm start
 
## 1. Implement product details page (branch: task_1)
  - Retrieve product id from url
  - Create new method in product-service.ts
  - Retrieve product using ngOnInit
  - Display all product attributes using *ngFor

## 2. List all product reviews (branch: task_2)
  - Create new ReviewsComponent
  - Create html template
  - Move html code responsible for displaying reviews from product-details.component.html to the new html template 
  - Create new ReviewService 

## 3.Implement add new review (branch: task_3)
  - Add new addReview method in ReviewsComponent
  - Add new addReview method reviews-service

## 4. Add search product functionality (branch: task_4)
   - Add <input class ="find-product" [(ngModel)]="searchTerm"/>
   - Add searchTerm field in component
   - Add | searchProducts:searchTerm" to the listing products template 
   - Create new ProductSearchPipe
   
## 5. Provide product and reviews data before route is activated - product details page (branch: task_5)
  - Create ProductResolve and ReviewResolve
  - Add resolve:{product: ProductResolve,reviews: ReviewResolve} to the dwfinitions of routes
  - Get the product data by invoking this.product = this.route.snapshot.data['product'];
  - Repeat the step for reviews
  - remove *ngIf="product" from product-details.component.html

